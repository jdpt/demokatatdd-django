from time import sleep

from selenium.webdriver.common.by import By
from unittest import TestCase
from selenium import webdriver


class Functionaltest(TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn('Busco Ayuda', self.browser.title)

    def test_registro(self):
        self.browser.get('http://127.0.0.1:8000/')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Juan')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Poveda')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('jdpt@example.com')

        experiencia = self.browser.find_element_by_id('id_experiencia')
        experiencia.send_keys('4')

        self.browser.find_element_by_xpath("//select[@id='id_servicio']/option[text()='Desarrollador']").click()

        telefono = self.browser.find_element_by_id('id_telefono')
        telefono.send_keys('3112211221')

        imagen = self.browser.find_element_by_id('id_foto')
        imagen.send_keys("C:\\limon.jpg")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys('jdpt')

        password = self.browser.find_element_by_id('id_password_confirm')
        password.send_keys('jdpt')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(4)
        span = self.browser.find_element(By.XPATH, '//span[text()="Juan Poveda"]')
        self.assertIn('Juan Poveda', span.text)

    def test_verdetalle(self):
        self.browser.get('http://127.0.0.1:8000/')
        span = self.browser.find_element(By.XPATH, '//div/span[text()="Juan Poveda"]/../img')
        span.click()

        self.browser.implicitly_wait(4)

        h2 = self.browser.find_element(By.XPATH, '//h3[text()="Juan Poveda"]')
        self.assertIn('Juan Poveda', h2.text)
