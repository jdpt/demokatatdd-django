from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login$', views.login_user_form, name='login_user'),
    url(r'^loginjs$', views.login_user_json, name='login_user'),
    url(r'^logout$', views.logout_user, name='logout_user'),
    url(r'^independiente/new$', views.new_independiente, name='newIndependiente'),
    url(r'^independiente/edit$', views.editaInformacion, name='editaInformacion'),
    url(r'^comentario$', views.new_comentario_json, name='newComentario'),
]