import json
import sys
from django.contrib.auth.models import User
from django.contrib.sessions import serializers
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

from models import Independiente, IndependienteForm, BaseIndependienteFormSet, BusquedaForm, Comentario
from django.contrib.auth import authenticate, login, logout
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, HttpResponseNotFound
from django.forms import inlineformset_factory

# Create your views here.


def index(request):
    if request.method == 'POST':
       lista_independientes = Independiente.objects.filter(correo=request.POST['correo'])
    else:
        lista_independientes = Independiente.objects.all()
    form = BusquedaForm()
    paginador = Paginator(lista_independientes, 4)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_independientes': lista_paginada, 'user_auth':request.user.is_authenticated(), 'form': form}

    return render(request, 'polls/index.html', context)


def new_independiente(request):
    if request.method == 'POST':
        form = IndependienteForm(request.POST, request.FILES)
        if form.is_valid():
            user = User.objects.create_user(form.instance.correo, form.instance.correo, request.POST.get('password'))
            user.save()

            form.instance.usuario = user
            form.save()
            return redirect(reverse('index'))
    else:
        form = IndependienteForm()

    return render(request, 'polls/independiente/new.html', {'form': form})

@csrf_exempt
def new_independiente_json(request):

    if request.method == 'POST':
        jsonIndependiente = json.loads(request.body)
        user = User.objects.create_user(jsonIndependiente['correo'], jsonIndependiente['correo'], jsonIndependiente['password'])
        user.save()

        independiente = Independiente()
        independiente.nombres = jsonIndependiente['nombres']
        independiente.apellidos = jsonIndependiente['apellidos']
        independiente.foto = jsonIndependiente['foto']
        independiente.experiencia = jsonIndependiente['experiencia']
        independiente.telefono = jsonIndependiente['telefono']
        independiente.servicio_id = jsonIndependiente['servicio_id']
        independiente.usuario = user
        independiente.save()

        return redirect(reverse('index'))

    return render(request, 'polls/independiente/new.html')


def editaInformacion(request):

    IndependienteFormSet = modelformset_factory(Independiente, formset=BaseIndependienteFormSet, fields=('nombres', 'apellidos','foto','experiencia','telefono','servicio'), extra=0, can_delete=False)

    data = {
     'form-TOTAL_FORMS': '1',
     'form-INITIAL_FORMS': '0',
     'form-MAX_NUM_FORMS': '',
    }
    formset = IndependienteFormSet(data)
    formset.is_valid()

    if request.method == 'POST':
        formset = IndependienteFormSet(request.POST)
        if formset.is_valid():
            formset.save()

            return redirect(reverse('index'))
    else:
        formset = IndependienteFormSet(queryset=Independiente.objects.filter(correo=request.user.email))

    return render(request, 'polls/independiente/edit.html',  {'formset': formset})


def login_user(request):
    if request.user.is_authenticated():
        return redirect('index')

    mensaje = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)

            return redirect(reverse('index'))
        else:
            mensaje = 'Nombre de usuario o clave no valido'

    return render(request, 'polls/login.html', {'mensaje': mensaje})

@csrf_exempt
def login_user_json(request):
    mensaje = ''
    if request.method == 'POST':
        jsonUser = json.loads(request.body)
        username = jsonUser['username']
        password = jsonUser['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            mensaje = 'ok'
        else:
            return HttpResponseNotFound('usuario / password no valido')

    return JsonResponse({'mensaje': mensaje})

def login_user_form(request):
    return render(request, 'polls/login.html')

def logout_user(request):
    logout(request)
    return redirect('index')

@csrf_exempt
def logout_user_json(request):
    logout(request)
    return JsonResponse({'mensaje': 'ok'})


def new_comentario(request):
    independiente =  Independiente.objects.get(id=request.POST.get('independiente'))

    comentario = Comentario()
    comentario.texto = request.POST.get('texto')
    comentario.independiente = independiente
    comentario.email = request.POST.get('email')
    comentario.save()
    return redirect('index')

@csrf_exempt
def new_comentario_json(request):

    if request.method == 'POST':
        jsonComentario = json.loads(request.body)
        independiente =  Independiente.objects.get(id=jsonComentario['independiente'])

        comentario = Comentario()
        comentario.texto = jsonComentario['texto']
        comentario.independiente = independiente
        comentario.email = jsonComentario['email']
        comentario.save()

        return JsonResponse({'mensaje': 'ok'})

def comentarios_json(request):
    lista_comentarios = Comentario.objects.filter(independiente=request.GET.get('independiente'))
    return HttpResponse(serializers.serialize('json', lista_comentarios))

@csrf_exempt
def isLogged_json(request):
    if request.user.is_authenticated():
       return JsonResponse({'mensaje': 'ok'})

    return JsonResponse({'mensaje': 'no'})