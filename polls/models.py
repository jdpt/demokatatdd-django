from __future__ import unicode_literals

from django import forms
from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User
from django.forms import ModelForm
from django.forms import BaseModelFormSet

# Create your models here.
class Servicio(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre


class Independiente(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    foto = models.ImageField(upload_to="buscoayuda/images")
    experiencia = models.IntegerField(default=0)
    telefono = models.CharField(max_length=100)
    correo = models.EmailField()
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, null=True)


    def __unicode__(self):
        return self.nombres + ' ' + self.apellidos


class IndependienteForm(ModelForm):

    experiencia=forms.IntegerField(label=("Experiencia"))
    password=forms.CharField(label=("Password"), widget=forms.PasswordInput)
    password_confirm=forms.CharField(label=("Password Confirmacion"), widget=forms.PasswordInput)

    class Meta:
        model=Independiente
        fields=['nombres', 'apellidos', 'correo', 'foto', 'telefono', 'servicio', 'experiencia']

    def clean_correo(self):
        data = self.cleaned_data['correo']
        if User.objects.filter(username=data).exists():
            raise ValidationError('Ya existe un usuario con este correo.')
        return data

    def clean_password_confirm(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password_confirm')

        if not password2:
            raise forms.ValidationError("Valor es requerido")
        if password1 != password2:
            raise forms.ValidationError("No coincide")
        return password2


class BusquedaForm(ModelForm):

    #correo = forms.CharField(required=False)

    class Meta:
        model=Independiente
        fields=['correo']

class BaseIndependienteFormSet(BaseModelFormSet):
    class Meta:
        model=Independiente
        fields=['nombres', 'apellidos', 'correo', 'foto', 'telefono', 'servicio', 'experiencia']


    def __init__(self, *args, **kwargs):
        super(BaseIndependienteFormSet, self).__init__(*args, **kwargs)

class Comentario (models.Model):
    independiente = models.ForeignKey(Independiente, on_delete=models.CASCADE)
    texto = models.CharField(max_length=5000)
    email = models.EmailField(default="email@email.com")

    def __unicode__(self):
        return self.independiente + ' ' + self.texto

